import { Locator, expect, Page } from '@playwright/test'

export class LoginPage {

    readonly page: Page
    readonly signInButton: Locator
    readonly userName: Locator
    readonly password: Locator
    readonly loginButton: Locator
    readonly remMeCheckBox: Locator
    readonly errMessage: Locator


    constructor(page: Page){

        this.page = page;
        this.signInButton = page.locator('#signin_button')
        this.userName = page.locator('#user_login')
        this.password = page.locator('#user_password')
        this.remMeCheckBox = page.locator('#user_remember_me')
        this.loginButton = page.locator('text=Sign in')  
        this.errMessage = page.locator('.alert-error')     
    }

    async goto() {
        await this.page.goto('http://zero.webappsecurity.com/index.html')
    }

    async clickSignButton(){
        await this.signInButton.click()
    }

    async enterUserName(username: string){
        await this.userName.fill(username)
    }

    async enterPassword(password: string){
        await this.password.fill(password)
    }

    async checkRemMe(check:string){
        if(check === 'check'){
            if (await !this.remMeCheckBox.isChecked){
                await this.remMeCheckBox.check()
            }
        }        
    }

    async clickLogin(){
        await this.loginButton.click()
    }

    async verifyErrorMessage(errMsg:string){
        await expect(this.errMessage).toContainText(errMsg)
    }



}