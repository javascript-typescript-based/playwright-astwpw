import {PlaywrightTestConfig} from '@playwright/test'

const config: PlaywrightTestConfig = {

    timeout: 60000,
    retries: 0,
    reporter: [
        ['html'],
        ['json', {  outputFile: './playwright-report/test-results.json' }],
        ['allure-playwright', {
            detail: true,
            outputFolder: 'my-allure-results',
            suiteTitle: false
          }],
      ],
    use: {
        headless : false,
        viewport : { width:1900, height:1100 },
        actionTimeout: 15000,        
        ignoreHTTPSErrors: true,
        video:"off",
        screenshot:"off",                  
    },
    projects: [
        {
            name:'Chromium',
            use: {browserName: 'chromium'}
        },
        {
            name:'Firefox',
            use: {browserName: 'firefox'}
        },
        {
            name:'Webkit',
            use: {browserName: 'webkit'}
        }
    ]

}

export default config