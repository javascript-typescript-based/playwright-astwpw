import { LoginPage } from '../package/pages/LoginPage'
import { test, expect } from '@playwright/test'

test('Hello', async({page}) => {
    const loginPage = new LoginPage(page);
    await loginPage.goto()
    await loginPage.clickSignButton()
    await loginPage.enterUserName('Hello')
    await loginPage.enterPassword('Hello')
    await loginPage.checkRemMe('check')
    await loginPage.clickLogin()
    await loginPage.verifyErrorMessage('Login and/or password are wrong.')
})