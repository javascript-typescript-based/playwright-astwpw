import {test, expect} from '@playwright/test'

test('simple test1 @regression', async({page}) => {
    await page.goto('http://zero.webappsecurity.com/index.html')
    await page.click('#signin_button')
    await page.type('#user_login', "Hello")
    await page.type('#user_password', "hello")
    await page.click("//input[@name='submit']")
    const errMessage = await page.locator('.alert-error')
    await expect(errMessage).toContainText('Login and/or password are wrong.')
})

test('simple test2 @sanity', async({page}) => {
    await page.goto('http://zero.webappsecurity.com/index.html')
    await page.click('#signin_button')
    await page.type('#user_login', "Hello")
    await page.type('#user_password', "hello")
    await page.click("//input[@name='submit']")
    const errMessage = await page.locator('.alert-error')
    await expect(errMessage).toContainText('Login and/or password are wrong.')
})