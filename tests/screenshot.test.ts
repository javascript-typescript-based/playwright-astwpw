import {expect, test } from '@playwright/test'


test('Screenshot Test', async({page}) => {
    await page.goto("https://google.com");
    await page.screenshot({path:'screenshot.png', fullPage:true})
})

test("Single Element Screenshot", async({page}) => {
    await page.goto("https://google.com");
    const element = page.locator("//input[@name='q']");
    await element.screenshot({path: 'single_element.png'});
})