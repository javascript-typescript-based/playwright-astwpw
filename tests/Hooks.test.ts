import { test, expect } from '@playwright/test'


test.describe.parallel.only('Hooks Examples', () => {

    test.beforeEach( async({page}) => {
       await page.goto('https://ecommerce-playground.lambdatest.io/index.php?route=account/register')
    })

    test('Enter FirstName', async({page}) => {
        await page.type('#input-firstname', 'Hello')
    })

    test('Enter LastName', async({page}) => {
        await page.type('#input-lastname', 'Hello')
    })

})